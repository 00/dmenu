/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 500;                    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Liberation:size=12:antialias:true"
};
static const char *prompt      = "^^ :";      /* -p  option; prompt to the left of input field */

static const unsigned int baralpha = 0xcc;
static const unsigned int borderalpha = OPAQUE;
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
  [SchemeNormHighlight]  = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
  [SchemeSelHighlight]  = { OPAQUE, baralpha, borderalpha },  
  [SchemeOut] = { OPAQUE, baralpha, borderalpha },
  [SchemeOutHighlight]  = { OPAQUE, baralpha, borderalpha },
  [SchemeHp] = { OPAQUE, baralpha, borderalpha },
};

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#B8B8B8", "#1D1B28" },
  [SchemeNormHighlight] = { "#FFFFFF", "#1D1B28" },
	[SchemeSel] = { "#FFFFFF", "#4462F5" },
  [SchemeSelHighlight] = { "#FFFFFF", "#1D1B28" },	
	[SchemeOut] = { "#000000", "#00ffff" },
  [SchemeOutHighlight] = { "#ffc978", "#00ffff" },
	[SchemeHp]  = { "#B8B8B8", "#1D1B28" }
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 2;
